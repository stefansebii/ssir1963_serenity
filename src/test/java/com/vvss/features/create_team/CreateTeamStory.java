package com.vvss.features.create_team;

import com.vvss.steps.serenity.EndUserSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

@RunWith(SerenityRunner.class)
public class CreateTeamStory {
    @Managed
    public WebDriver webDriver;

    @Steps
    public EndUserSteps anna;

    @Test
    public void create_valid_team(){
        anna.is_the_home_page();
        anna.enters_credentials("frodobaggins13", "password");
        anna.presses_login();
        anna.opens_create_team_prompt();
        anna.types_team_name("Team-test");
        anna.press_create_team_button();
        anna.should_see_edit_team_profile_button();
    }

    @Test
    public void try_to_create_team_no_name(){
        anna.is_the_home_page();
        anna.enters_credentials("frodobaggins13", "password");
        anna.presses_login();
        anna.opens_create_team_prompt();
        anna.types_team_name("");
        anna.create_team_button_should_be_disabled();
    }
}
