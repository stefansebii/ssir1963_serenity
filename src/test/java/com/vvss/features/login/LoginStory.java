package com.vvss.features.login;

import com.vvss.steps.serenity.EndUserSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

@RunWith(SerenityRunner.class)
public class LoginStory {
    @Managed
    public WebDriver webDriver;

    @Steps
    public EndUserSteps anna;

    @Test
    public void empty_email_and_password(){
        anna.is_the_home_page();
        anna.enters_credentials("", "");
        anna.presses_login();
        anna.should_see_error_message("Missing email");
    }

    @Test
    public void valid_login(){
        anna.is_the_home_page();
        anna.enters_credentials("frodobaggins13", "password");
        anna.presses_login();
    }
}
