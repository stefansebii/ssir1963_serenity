package com.vvss.pages;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;

import java.util.concurrent.TimeUnit;

@DefaultUrl("https://trello.com/login")
public class TrelloHomePage extends PageObject {
    @FindBy(name="user")
    private WebElementFacade usernameBox;

    @FindBy(name="password")
    private WebElementFacade passwordBox;

    @FindBy(id="login")
    private WebElementFacade loginButton;

    public String error_message_displays(){
        WebElementFacade error_box = find(By.id("error"));
        return error_box.getText();
    }

    public void enter_username(String username){
        usernameBox.type(username);
    }

    public void enter_password(String password){
        passwordBox.type(password);
    }

    public void press_login(){
        loginButton.click();
    }


    public void open_new_team_prompt() {
        setImplicitTimeout(15, TimeUnit.SECONDS);
        WebElementFacade createMenu = find(By.className("js-open-add-menu"));
        createMenu.click();

        WebElementFacade createTeamButton = find(By.xpath("//*[contains(text(), 'Create Team')]"));
        createTeamButton.click();
    }

    public void insert_team_name(String name){
        WebElementFacade input = find(By.id("org-display-name"));
        input.type(name);
    }

    public void press_create_team(){
        WebElementFacade createTeamButton = find(By.className("js-save"));
        createTeamButton.click();
    }

    public boolean edit_team_profile_button_visible(){
        return isElementVisible(By.xpath("//*[contains(text(), 'Edit Team Profile')]"));
    }

    public boolean create_team_button_disabled(){
        WebElementFacade createTeamButton = find(By.className("js-save"));
        String attr = createTeamButton.getAttribute("disabled");
        return Boolean.valueOf(attr);
    }
}

