package com.vvss.steps.serenity;

import com.vvss.pages.TrelloHomePage;
import net.thucydides.core.annotations.Step;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;

public class EndUserSteps {

    private TrelloHomePage trelloHomePage;

    @Step
    public void enters_credentials(String username, String password){
        trelloHomePage.enter_username(username);
        trelloHomePage.enter_password(password);
    }

    @Step
    public void presses_login(){
        trelloHomePage.press_login();
    }

    @Step
    public void is_the_home_page() {
        trelloHomePage.open();
    }

    @Step
    public void should_see_error_message(String message){
        assertThat(trelloHomePage.error_message_displays(), containsString(message));
    }

    @Step
    public void opens_create_team_prompt(){
        trelloHomePage.open_new_team_prompt();
    }

    @Step
    public void types_team_name(String name){
        trelloHomePage.insert_team_name(name);
    }

    @Step
    public void press_create_team_button(){
        trelloHomePage.press_create_team();
    }

    @Step
    public void should_see_edit_team_profile_button(){
        assertThat(trelloHomePage.edit_team_profile_button_visible(), is(true));
    }

    @Step
    public void create_team_button_should_be_disabled(){
        assertThat(trelloHomePage.create_team_button_disabled(), is(true));
    }
}